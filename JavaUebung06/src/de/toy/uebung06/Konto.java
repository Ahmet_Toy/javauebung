package de.toy.uebung06;

public class Konto {
	
	private String kontonummer;
	private double kontostand;
	
	Konto(String knr, double ks){	// Konstruktor
		kontonummer = knr;
		kontostand = ks;
	}
	
	String getKontonummer() {
		return kontonummer;
	}
	
	double getKontostand() {
		return kontostand;
	}
	
	void einzahlen(double betrag) {
		kontostand = kontostand + betrag;
	}
	
	void auszahlen(double betrag) {
		kontostand = kontostand - betrag;
	}
}
