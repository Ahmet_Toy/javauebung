package de.toy.uebung06;

public class Girokonto extends Konto {

	double limit;

	Girokonto(String knr, double ks, double klim) {
		super(knr, ks);
		limit = klim;
	}

	double getLimit() {
		return limit;
	}

	void setLimit(double klim) {
		limit = klim;
	}

	void auszahlen(double betrag) {
		if ((getKontostand() - betrag) >= limit *(-1)) {		//*(-1) weil der Kontostand - Auszahlungsbetrag gr��er sein muss als der neg. Limitbetrag.
			super.auszahlen(betrag);		//f�hrt die Methode auszahlen() in der Superklasse Konto aus.
		} else {
			System.out.println("Fehler! Auszahlung nicht m�glich, da Kreditlimit �berschritten.");
		}
	}

}
