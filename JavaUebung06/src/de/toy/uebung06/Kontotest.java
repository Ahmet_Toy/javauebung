package de.toy.uebung06;

public class Kontotest {

	public static void main(String[] args) {

		var k = new Konto("0000000001", 1000);

		String kontonummerAusgeben = k.getKontonummer();
		System.out.println("Ihre Kontonummer: " + kontonummerAusgeben);

		double kontostandAusgeben = k.getKontostand();
		System.out.println("Ihr Kontostand: " + kontostandAusgeben);

		k.einzahlen(500.00);

		k.auszahlen(750.50);

		System.out.println("Ihre Kontonummer: " + k.getKontonummer());
		System.out.println("Ihr neuer Kontostand: " + k.getKontostand());
	}

}
