public class Kreisberechnung3 {
	public static void main (String[] args) {  // Das ist die main-Methode, welches als Einstiegspunkt in ein Java-Programm dient.
		var einheit = args[1];
		var radius = Double.parseDouble(args[0]);
		var umfang = 2.0 * 3.1415926 * radius;
		var inhalt = 3.1415926 * radius * radius;
		System.out.print("Umfang: ");
		System.out.print(umfang);
		system.out.println(" " + einheit);
		System.out.print("Flaeche: ");
		System.out.print(inhalt);
		System.out.println(" " + einheit + '\u00b2');	// durch '\u00b2' wird die Ausgabe der Hochstelle in der Einheit (z.B. m^2) erreicht
	}
}