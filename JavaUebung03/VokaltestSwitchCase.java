/* Programm zum testen auf Vokale mit der switch-case Anweisung = Mehrseitige Auswahlstruktur
*
*@date 09.09.2021
*/

import javax.swing.JOptionPane;

public class VokaltestSwitchCase {
	public static void main(String[] args) {
	
	var eingabe = JOptionPane.showInputDialog("Geben Sie einen Buchstaben ein: ");
	var c = eingabe.charAt(0);
	switch (c) {		//für den Fall, dass...
		case 'a':		// kein break, da folgend weitere Konstanten mit dem Ausdruck c verglichen werden sollen
		case 'e':
		case 'i':
		case 'o':
		case 'u': JOptionPane.showMessageDialog(null, c + "ist ein Vokal!");
		break;
		default: JOptionPane.showMessageDialog(null, c + " ist kein Vokal!");
		}
	}
}
		