/* Programm zum testen auf Vokale 
*
*@date 09.09.2021
*/

import javax.swing.JOptionPane;

public class Vokaltest {
	public static void main(String[] args) {
		var istVokal = false;		//Initialisierung
		var eingabe = JOptionPane.showInputDialog("Geben Sie einen Buchstaben ein; ");
		
		var c = eingabe.charAt(0);	// Methode charAt gibt aus einem String ein einzelnes Zeichen des Strings zurück, da JOptionPane immer einen String zurückgibt 
		if (c == 'a') {				// charAt(0) -- die 0 gibt an, dass die Methode das erste Zeichen des Strings zurückgeben soll.
			istVokal = true;
		} else {
			if (c == 'e') {
				istVokal = true;
			} else {
				if (c == 'i') {
					istVokal = true;
				} else {
					if (c == 'o') {
						istVokal = true;
					} else {
						if (c == 'u') {
							istVokal = true;
						} else {
							istVokal = false;
						}
					}
				}
			}
		}
		if (istVokal) {
			JOptionPane.showMessageDialog(null, c + "ist ein Vokal!");
		} else {
			JOptionPane.showMessageDialog(null, c + "ist kein Vokal!");
		}
	}
}
					
	