/* Java - Syntax der verschiedenen Schleifenarten (while, do, for)
*
*Date: 09.09.21
*/

// While-Schleife: Sie ist kopfgesteuert, d.h. zuerst wird die Bedingung geprüft und dann folgen die Anweisungen

public class BeispielWhile {
	public static void main(String[] args) {
		
		var zahl = 0;
		
		while(zahl<101) {
			System.out.println(zahl);
			zahl++;
		}
	}
}

// Do-Schleife: Sie ist wie die while-Schleife, nur fußgesteuert, d.h. zuerst werden die Anweisungen ausgeführt und danach erst die Bedingung geprüft

public class BeispielDo {
	public static void main(String[] args) {
		
		var zahl = 0;
		do {
			System.out.println(zahl);
			zahl++;
		} while (zahl < 101);
	}
}

/* For-Schleife: Sie dient dazu die Anweisungsfolge abhängig von Kontrollausdrücken durchzuführen (Iteration)
	Syntax: for (Initialisierung; Bedingung; Aktualisierung) {
			   Anweisungsfolge
			}

*/

public class BeispielFor {
	public static void main(String[] args) {
		
		for(var zahl = 0; zahl<101; zahl++) {
			System.out.println(zahl);
		}
	}
}