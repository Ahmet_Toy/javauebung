/* Aufgabe Zahlraten S. 114
*
*Date 09.09.21
*/

input.javax.swing.JOptionPane;

public class Zahlraten {
	public static void main(String[] args) {
	
	var zahl = 6;
	var eingabe = JOptionPane.showInputDialog("Erraten Sie eine Zahl zwischen 0 und 10!");
	
	var erraten = Integer.parseInt(eingabe);	// wandelt einen String in einen Integer um
	
	switch(erraten) {
		case 1:
		case 2:
		case 3:	
		case 9:
		case 10:	JOptionPane.showMessageDialog(null, "Daneben");
					break;
		case 4:
		case 5: 
		case 7:
		case 8:		JOptionPane.showMessageDialog(null, "Knapp daneben";
					break;
		default:	JOptionPane.showMessageDialog(null, "Treffer!");
		}
	}
}