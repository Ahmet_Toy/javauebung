/* Übungsaufgaben 3.3.5 Aufgabe 1 : Zahlendreieck
*
*Date: 10.09.2021
*/

public class ZahlendreieckWhile {
	public static void main(String[] args) {
		
		var zahl = 0;
		var ausgabe = "";
		while(0 < 10) {
			ausgabe = ausgabe + zahl;
			System.out.println(ausgabe);
			zahl++;
		}
	}	
}		