/* Java-Übung Aufgabe 1 Seite 110
*
*Date: 09.09.2021
*/

import javax.swing.JOptionPane;

public class Zahlentest{
	
	public static void main(String[] args) {
		
		var istPositiv = false;
		var eingabe = JOptionPane.showInputDialog(" Geben Sie eine beliebige Zahl ein!");
		
		var zahl = Double.parseDouble(eingabe);
		
		if ( zahl >= 0) {
			istPositive = true;
		} else {
			istPositiv = false;
		}
		if (istPositiv) {
			JOptionPane.showMessageDialog(null, "Die Zahl" + zahl + "ist positiv");
		} else {
			JOptionPane.showMessageDialog(null, " Die Zahl " + zahl + "ist negativ");
		}
	}
}	
	
	