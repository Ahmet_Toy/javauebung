package de.toy.uebung10;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import java.text.DecimalFormat;

public class Notenbilanz extends JFrame {

    private JPanel contentPane;
    
    private JTextField tfAnzahlNoten;
    private JTextField tfNote;
    private JTextField tfNotennummer;
    private JTextField tfNeuerWert;
    
    private JLabel lblNote;
    private JLabel lblNotenzahl;
    private JLabel lblNotenschnitt;
    private JLabel lblBesteNote;
    private JLabel lblSchlechtesteNote;
    private JLabel lblNoten_Array;
    private JLabel lblNeuerWert;
    private JLabel lblNotennummer;
    
    private JButton btnAnzahlUebernehmen;
    private JButton btnNoteUebernehmen;
    private JButton btnNeu;
    private JButton btnNotennummer;
    private JButton btnNeuenWertSpeichern;
    
    private int notenzahl, i, notennummer;
    private double summe, besteNote, schlechtesteNote, notenschnitt;
    private double[] noten;
    
    private DecimalFormat fzahl;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
    	try {
    		UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
    	} catch (Throwable e) {
    		e.printStackTrace();
    	}
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    Notenbilanz frame = new Notenbilanz();
		    frame.setVisible(true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create the frame.
     */
    public Notenbilanz() {
    	
    	Font arial = new Font("Arial", Font.PLAIN + Font.ITALIC, 12);
    	DecimalFormat fzahl = new DecimalFormat("###,##0.00");
    	
	    setTitle("Notenbilanz");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 533, 327);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAnzahlNoten = new JLabel("Anzahl Noten");
		lblAnzahlNoten.setBounds(10, 10, 81, 20);
		lblAnzahlNoten.setFont(arial);
		contentPane.add(lblAnzahlNoten);
		
		tfAnzahlNoten = new JTextField();
		tfAnzahlNoten.setBounds(10, 30, 86, 25);
		tfAnzahlNoten.setFont(arial);
		contentPane.add(tfAnzahlNoten);
		tfAnzahlNoten.setColumns(10);
		
		btnAnzahlUebernehmen = new JButton("\u00DCbernehmen");
		btnAnzahlUebernehmen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    if (!tfAnzahlNoten.getText().equals("")) {
				    notenzahl = Integer.parseInt(tfAnzahlNoten.getText());
				    noten = new double[notenzahl];
				    i = 1;
				    summe = 0;
				    besteNote = 6;
				    schlechtesteNote = 0;
				    btnAnzahlUebernehmen.setVisible(false);
				    tfAnzahlNoten.setEditable(false);
				    lblNote.setVisible(true);
				    tfNote.setVisible(true);
				    btnNoteUebernehmen.setVisible(true);
				    tfNote.requestFocus();
				} else {
				    JOptionPane.showMessageDialog(null, "Anzahl der Noten eingeben!");
				    tfAnzahlNoten.requestFocus();
				}
			}
		});
		btnAnzahlUebernehmen.setBounds(112, 30, 134, 25);
		btnAnzahlUebernehmen.setFont(arial);
		contentPane.add(btnAnzahlUebernehmen);
		
		JButton btnEnde = new JButton("Ende");
		btnEnde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    System.exit(0);
			}
		});
		btnEnde.setBounds(420, 257, 89, 25);
		btnEnde.setFont(arial);
		contentPane.add(btnEnde);
		
		lblNote = new JLabel("1.Note");
		lblNote.setVisible(false);
		lblNote.setBounds(10, 70, 81, 20);
		lblNote.setFont(arial);
		contentPane.add(lblNote);
		
		tfNote = new JTextField();
		tfNote.setVisible(false);
		tfNote.setBounds(10, 90, 86, 25);
		tfNote.setFont(arial);
		contentPane.add(tfNote);
		tfNote.setColumns(10);
		
		btnNoteUebernehmen = new JButton("\u00DCbernehmen");
		btnNoteUebernehmen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    if (!tfNote.getText().equals("")
					&& Double.parseDouble(tfNote.getText()) >= 1
					&& Double.parseDouble(tfNote.getText()) <= 6) {
				    summe = summe + Double.parseDouble(tfNote.getText());
				    noten[i-1] = Double.parseDouble(tfNote.getText());
				    notenschnitt = summe / i;
				    lblNotenschnitt.setText("Notenschnitt: "
					    + fzahl.format(notenschnitt));
				    lblNotenzahl.setText("Anzahl eingegebene Noten: "
					    + Integer.toString(i));
	
				    if (Double.parseDouble(tfNote.getText()) > schlechtesteNote) {
					schlechtesteNote = Double.parseDouble(tfNote.getText());
					lblSchlechtesteNote.setText("schlechteste Note: "
						+ Double.toString(schlechtesteNote));
				    }
				    if (Double.parseDouble(tfNote.getText()) < besteNote) {
					besteNote = Double.parseDouble(tfNote.getText());
					lblBesteNote
						.setText("beste Note: " + Double.toString(besteNote));
				    }
				    if (i == notenzahl) {
					notenschnitt = summe / notenzahl;
					btnNoteUebernehmen.setVisible(false);
					btnNeu.setVisible(true);
					lblNoten_Array.setText("Noten: ");
					for (var i=0; i<noten.length; i++) {
					  lblNoten_Array.setText(lblNoten_Array.getText() + "  " + noten[i]);
					}
					lblNotennummer.setVisible(true);
					tfNotennummer.setVisible(true);
					btnNotennummer.setVisible(true);
	
				    } else {
					i++;
					lblNote.setText(Integer.toString(i) + ". Note");
					tfNote.setText("");
					tfNote.requestFocus();
				    }
				} else {
				    JOptionPane.showMessageDialog(null, "g�ltige Note eingeben!");
				    tfNote.requestFocus();
				}
			}
		});
		btnNoteUebernehmen.setVisible(false);
		btnNoteUebernehmen.setBounds(117, 90, 129, 25);
		btnNoteUebernehmen.setFont(arial);
		contentPane.add(btnNoteUebernehmen);
		
		lblNotenzahl = new JLabel("");
		lblNotenzahl.setBounds(10, 120, 236, 14);
		lblNotenzahl.setFont(arial);
		contentPane.add(lblNotenzahl);
		
		lblNotenschnitt = new JLabel("");
		lblNotenschnitt.setBounds(10, 145, 236, 14);
		lblNotenschnitt.setFont(arial);
		contentPane.add(lblNotenschnitt);
		
		lblBesteNote = new JLabel("");
		lblBesteNote.setBounds(10, 170, 236, 14);
		lblBesteNote.setFont(arial);
		contentPane.add(lblBesteNote);
		
		lblSchlechtesteNote = new JLabel("");
		lblSchlechtesteNote.setBounds(10, 198, 236, 14);
		lblSchlechtesteNote.setFont(arial);
		contentPane.add(lblSchlechtesteNote);
		
		btnNeu = new JButton("Neue Berechnung");
		btnNeu.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			i = 1;
			lblNote.setText(Integer.toString(i) + ".Note");
			summe = 0;
			besteNote = 6;
			schlechtesteNote = 0;
			btnAnzahlUebernehmen.setVisible(true);
			tfAnzahlNoten.setEditable(true);
			lblNote.setVisible(false);
			tfNote.setVisible(false);
			tfNote.setText("");
			btnNoteUebernehmen.setVisible(false);
			tfAnzahlNoten.setText("");
			tfAnzahlNoten.requestFocus();
			lblNotenschnitt.setText("");
			lblBesteNote.setText("");
			lblSchlechtesteNote.setText("");
			lblNotenzahl.setText("");
			btnNeu.setVisible(false);
			lblNoten_Array.setText("");
			lblNotennummer.setVisible(false);
			tfNotennummer.setText("1");
			tfNotennummer.setVisible(false);
			btnNotennummer.setVisible(false);
			lblNeuerWert.setVisible(false);
			tfNeuerWert.setVisible(false);
			btnNeuenWertSpeichern.setVisible(false);
		    }
		});
		btnNeu.setVisible(false);
		btnNeu.setBounds(313, 30, 168, 25);
		btnNeu.setFont(arial);
		contentPane.add(btnNeu);
		
		lblNoten_Array = new JLabel("");
		lblNoten_Array.setBounds(10, 225, 414, 14);
		lblNoten_Array.setFont(arial);
		contentPane.add(lblNoten_Array);
		
		tfNotennummer = new JTextField();
		tfNotennummer.setVisible(false);
		tfNotennummer.setBounds(288, 90, 33, 25);
		tfNotennummer.setFont(arial);
		contentPane.add(tfNotennummer);
		tfNotennummer.setColumns(10);
		
		lblNotennummer = new JLabel(".Note bearbeiten");
		lblNotennummer.setVisible(false);
		lblNotennummer.setBounds(322, 90, 86, 25);
		lblNotennummer.setFont(arial);
		contentPane.add(lblNotennummer);
		
		btnNotennummer = new JButton("OK");
		btnNotennummer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    try {
				    notennummer = Integer.parseInt(tfNotennummer.getText()) - 1;
				    if ((notennummer >= 0) && (notennummer < notenzahl)) {
					lblNeuerWert.setText(notennummer + 1 + ". Note");
					lblNeuerWert.setVisible(true);
					tfNeuerWert.setVisible(true);
					btnNeuenWertSpeichern.setVisible(true);
					tfNeuerWert.setText(String.valueOf(noten[notennummer]));
					tfNeuerWert.requestFocus();
				    } else {
					JOptionPane.showMessageDialog(null,
						"Notennummer au�erhalb des g�ltigen Bereichs!");
					tfNotennummer.requestFocus();
				    }
				} catch (Exception ex) {
				    JOptionPane.showMessageDialog(null, "ung�ltiges Zahlenformat!");
				}
			}
		});
		btnNotennummer.setVisible(false);
		btnNotennummer.setBounds(426, 90, 55, 25);
		btnNotennummer.setFont(arial);
		contentPane.add(btnNotennummer);
		
		lblNeuerWert = new JLabel("1. Note");
		lblNeuerWert.setVisible(false);
		lblNeuerWert.setBounds(280, 130, 46, 20);
		lblNeuerWert.setFont(arial);
		contentPane.add(lblNeuerWert);
		
		tfNeuerWert = new JTextField();
		tfNeuerWert.setVisible(false);
		tfNeuerWert.setBounds(280, 150, 60, 25);
		tfNeuerWert.setFont(arial);
		contentPane.add(tfNeuerWert);
		tfNeuerWert.setColumns(10);
		
		btnNeuenWertSpeichern = new JButton("Speichern");
		btnNeuenWertSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    try {
				    noten[notennummer] = Double.parseDouble(tfNeuerWert.getText());
				    summe = 0;
				    besteNote = noten[0];
				    schlechtesteNote = noten[0];
				    lblNoten_Array.setText("Noten: ");
				    for (double x : noten) {
					summe = summe + x;
					if (besteNote > x) {
					    besteNote = x;
					}    
					if (schlechtesteNote < x) {
					    schlechtesteNote = x;
					}   
					lblNoten_Array.setText(lblNoten_Array.getText() + " " + x);
				    }
				    notenschnitt = summe / notenzahl;
				    lblBesteNote.setText("beste Note: " + Double.toString(besteNote));
				    lblSchlechtesteNote.setText("schlechteste Note: "
					    + Double.toString(schlechtesteNote));
				    lblNotenschnitt.setText("Notenschnitt: "
					    + Double.toString(notenschnitt));
				} catch (Exception ex) {
				    JOptionPane.showMessageDialog(null, "ung�ltiges Zahlenformat!");
				}
			}
		});
		btnNeuenWertSpeichern.setVisible(false);
		btnNeuenWertSpeichern.setBounds(350, 150, 131, 25);
		btnNeuenWertSpeichern.setFont(arial);
		contentPane.add(btnNeuenWertSpeichern);
	    }
	}
