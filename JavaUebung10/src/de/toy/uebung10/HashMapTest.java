package de.toy.uebung10;

import java.util.HashMap;


public class HashMapTest {

	public static void main(String[] args) {
		
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		
		map.put(1, "Berlin");
		map.put(2, "Haburg");
		map.put(3, "M�nchen");
		map.put(4, "K�ln");
		map.put(58,"Sivas");
		
		System.out.println("Size: " + map.size() + "\n");
		
		map.forEach((k, v) -> System.out.println("Schl�ssel: " + k + " Wert: " + v));

	}
}
