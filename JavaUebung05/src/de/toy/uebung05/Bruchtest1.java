package de.toy.uebung05;

public class Bruchtest1 {

	public static void main(String[] args) {
		
		Bruch b = new Bruch();	// statt var b = wird der Name "Bruch" verwendet. Damit wird eine Variable deklariert, die als Typ eine Klasse (Bruch) verwendet. b stellt damit einen Objekt dar.
		b.zaehler = 3;
		b.nenner = 12;
		System.out.println("Bruch b = " + b.zaehler + "/" + b.nenner);
		
		Bruch a = new Bruch(); // Hier wird ein neues Objekt definiert mit den selben Werten f�r Z�hler und Nenner. Eine einfache Zuweisung der Werte wie bei primitiven Typen (z.B. int) w�rde dazu f�hren, dass das Objekt (b) und seine Attribute lediglich eine zweite Bezeichnung erhalten, der auf das selbe Objekt verweist. 
		a.zaehler = 3;
		a.nenner = 12;
		System.out.println("Bruch a = " + a.zaehler + "/" + a.nenner);
		
		b.ausgeben();	// Mit dieser Anweisung wird die Instanz-Methode "ausgabe()" des Objekts "Bruch" abgearbeitet
		
		System.out.print("\n Und nach dem K�rzen: ");	// das Escape-Symbol: \n f�gt einen Zeilenumbruch ein
		b.kuerzen();
		b.ausgeben();
		
		b.gekuerztausgeben();	// b.kuerzen und b.ausgeben kann durch diese neue Methode, die aus sich heraus andere Methoden aufruft, ersetzt werden
		
		b.erweitern(4, 2.35649811);	// �bergabeparameter f�r die Methode erweitern
		
		System.out.println(a.dezimalwert());	// Mit der print-Anweisung wird die Methode dezimalwert aufgerufen. Diese ben�tigt keine Parameter ben�tigt, da die Methode z�hler und nenner der Instanz a als double zur�ckgibt
		
	}	
}