package de.toy.uebung05;

import javax.swing.JOptionPane;

public class Rechtecktest {

	public static void main(String[] args) {
		
		var eingabe = JOptionPane.showInputDialog("Geben Sie eine L�nge ein: ");
		double l = Double.parseDouble(eingabe);
		
		eingabe = JOptionPane.showInputDialog("Geben Sie eine Breite ein: ");
		double b = Double.parseDouble(eingabe);
		
		var einheit = JOptionPane.showInputDialog("Geben Sie die Ma�einheit ein: ");
		
		Rechteck r = new Rechteck(l, b);
		
		double langeSeite = r.getLangeSeite();
		double kurzeSeite = r.getKurzeSeite();
		double diagonale  = r.getDiagonale();
		double umfang = r.getUmfang();
		double flaeche = r.getFlaeche();
		
		System.out.print("Lange Seite: ");
		System.out.printf("%1.2f", langeSeite);
		System.out.println(" " + einheit);
		
		System.out.print("Kurze Seite: ");
		System.out.printf("%1.2f", kurzeSeite);
		System.out.println(" " + einheit);
		
		System.out.print("Diagonale: ");
		System.out.printf("%1.2f", diagonale);
		System.out.println(" " + einheit);
		
		System.out.print("Umfang: ");
		System.out.printf("%1.2f", umfang);
		System.out.println(" " + einheit);
		
		System.out.print("Fl�che: ");
		System.out.printf("%1.2f", flaeche);
		System.out.println(" " + einheit);
		
		// Aufgabe 3:
		r.laengeAusgeben();
		
		// Aufgabe 4:
		r.laengeVergroessern(5.8);
		r.breiteVerkleinern(1.5);
		
		System.out.println("Vergr��erte L�nge: " + langeSeite);		//liefert noch nicht korrektes Ergebnis
		System.out.println("Verkleinerte Breite: " + kurzeSeite);	//liefert noch nicht korrektes Ergebnis
	}

}
