package de.toy.uebung05;

public class Rechteck {
		
	double laenge;
	double breite;
		
	Rechteck() {
		laenge = 0;
		breite = 0;		
	}
	Rechteck(double l, double b) {
		this.laenge = l;
		this.breite = b;
	}
	double getLaenge() {
		return laenge;
	}
	void setLaenge(double l) {
		this.laenge = l;
	}
	double getBreite() {
		return breite;
	}
	void setBreite(double b) {
		this.breite = b;
	}
	double getLangeSeite() {
		if (laenge > breite) {
			return laenge;
		} else {
			return breite;	
		}
	}
	double getKurzeSeite() {
		if (laenge > breite) {
			return breite;
		} else {
				return laenge;
		}
	}
	void setSeiten(double l, double b) {
		this.laenge = l;
		this.breite = b;
	}
	double getDiagonale() {
		return Math.sqrt(Math.pow(laenge, 2) + Math.pow(breite, 2));
	}
	double getFlaeche() {
		return laenge * breite;
	}
	double getUmfang() {
		return 2 * laenge + 2 * breite;
	}
	// Aufgabe 3:
	void laengeAusgeben() {
		double laenge = 5.4;	//Variable wird nicht als Fehler markiert, dessen Namen als Attribute bereits verwendet wird
		System.out.println("L�nge: " + laenge);
	}
	//Aufgabe 4:
	void laengeVergroessern(double ll) {
		laenge += ll; 
	}
	void breiteVergroessern(double bb) {
		breite += bb;
	}
	void laengeVerkleinern(double ll ) {
		laenge -= ll;
	}
	void breiteVerkleinern(double bb) {
		breite -= bb;
	}
}


