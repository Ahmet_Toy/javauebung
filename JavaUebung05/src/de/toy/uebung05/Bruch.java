package de.toy.uebung05;

public class Bruch {
	int zaehler;
	int nenner;
	
	void ausgeben() {	// Kopf der Methode mit der Instanzmethode "ausgeben"
		System.out.print (zaehler + "/" + nenner);	//Rumpf der Methode, in dem das auszuf�hrende Programm enthalten ist		
	}
	
	void kuerzen() {
		var m = Math.abs(zaehler); 	// Math.abs gibt immer nur den Absolutwert einer zahl aus, d.h. sie macht aus negativen Zahlen -> positive Zahlen und speichert sie in m
		var n = Math.abs(nenner);	// speichert in n den Betrag (Absolutwert) von nenner
		var r = m % n;				// % - Modulo-Operator: sie dividiert Ganzzahlen und gibt den Restwert zur�ck
		while (r > 0) {
			m = n;
			n = r;
			r = m % n;
		}
		zaehler /= n;		// in n steht jetzt der gr��te gemeinsame Teiler (ggt)
		nenner /= n;
	}
	
	void gekuerztausgeben() {  // Diese Methode ruft die anderen Methoden kuerzen und ausgeben der Klasse auf
		kuerzen();				// f�hrt die Methode void kuerzen aus 
		ausgeben();				// f�hrt die Methode void ausgeben aus
	}
	
	void erweitern(int a, double b) {	// Methoden mit Parameter
		zaehler *= a;	// bedeuet zaehler = zaehler * a
		nenner *= b;	
	}
	
	void multipliziere(Bruch m) {	// hier wird ein Referenztyp, also ein Objekt, als Parameter �bergeben. Es wird jedoch kein neues Objekt m definert, stattdessen stellt m nur einen Verweis f�r das beim Aufruf verwendete Argument dar.
		zaehler *= m.zaehler;
		nenner *= m.nenner;
	}
	
	double dezimalwert() {
		return zaehler/nenner;	// Hier wird eine Methode mit Ergebnisr�ckgabe definiert, in der anstelle von void (der nichts zur�ckgibt) der Datentyp des R�ckgabewertes angegeben wird (hier double) und mit der return -Anweisung der Wert des dahinterstehenden Ausdrucks an das Programm zur�ckgelierfert wird.
	}
}