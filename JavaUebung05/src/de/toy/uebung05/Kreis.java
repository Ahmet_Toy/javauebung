package de.toy.uebung05;

public class Kreis {
	double radius;
	
	Kreis() {
		radius = 0;
	}
	Kreis(double r) {
		radius = r;
	}
	double getRadius() {
		return radius;
	}
	void setRadius(double r) {
		radius = r;
	}
	double getUmfang() {
		return radius * 2 * Math.PI; // Math.PI steht f�r die Kreiszahl Pi.
	}
	double getFlaeche() {
		return radius * radius * Math.PI;
	}
	void setUmfang(double u) {
		radius = u / (2 * Math.PI); 
	}
}