public class Variablen2 {
	public static void main(String[] args) {
		
		// Initialisierungen
		char z1 = 'a';
		char z2 = 'b';
		char z3 = 'A';
		char z4 = 0x00a9	// Der Unicode U+00A9 steht für das Copyright Zeichen, wird jedoch beginnend mit 0x... geschrieben, damit der Compiler es auswerten kann
		char z5 = 0x00be	// Unicode für das 3/4 Zeichen.
		
		//Ausgaben
		System.out.print("z1: ");
		System.out.println(z1);
		System.out.print("z2: ");
		System.out.println(z2);
		System.out.print("z3: ");
		System.out.println(z3);
		System.out.print("z4:");
		System.out.println(z4);
		System.out.print("z5: ");
		System.out.println(z5);
	}
}