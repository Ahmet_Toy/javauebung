public class Variablen1 {
	public static void main(String[] args) {
		
		//Deklaration und Initialisierungen
		byte bZahl = 28;
		short sZahl = -18453;
		int iZahl = 4356576;
		long lZahl = 345236577970L;		// Der Buchstabe L wird hinten angehängt, da der Compiler ihn sonst nicht als long erkennt sondern als int
		float fZhal = 4.37456678F;		// ebenso bei float ein F angehängt, da der Compiler ihn sonst standardmäßig als double erkennt. 
		double dZahl = 3645.564782;
		boolean bestanden = true;
		char zeichen = %;
		
		// Ausgabe der Variablenwerte
		System.out.print("bZahl=");
		System.out.println(bZahl);
		System.out.print("sZahl=");
		System.out.println(sZahl);
		System.out.print("iZahl=");
		System.out.println(iZahl);
		System.out.print("lZahl=");
		System.out.println(lZahl);
		System.out.print("fZahl=");
		System.out.println(fZahl);
		System.out.print("dZahl=");
		System.out.println(dZahl);
		System.out.print("bestanden=");
		System.out.println(bestanden);
		System.out.print("zeichen=");
		System.out.println(zeichen);
	}
}