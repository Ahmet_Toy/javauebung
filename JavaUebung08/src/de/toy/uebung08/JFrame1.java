package de.toy.uebung08;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;

public class JFrame1 extends JFrame {
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() { // main-Methode startet mit EventQueue... einen lauff�higen Thread =
												// parallel laufender Teil eines Prozesses
			public void run() { // run-Methode des Runnable-Objekts wird nur ausgef�hrt, wenn sie das Ende der
								// Ereigniswarteschlange erreicht
				try {
					JFrame1 frame = new JFrame1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFrame1() {
		setTitle("Nutzloses Programm");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Bei anklicken des x-Symbols wird das Frame geschlossen und
														// das Programm beendet
		setBounds(100, 100, 784, 310); // erste Werte-Paar ist x- und y-Koordinate, also die Position. Das zweite
										// Werte-Paar beschreibt Breite x H�he.
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblAnsage = new JLabel(
				"Ey dieses Programm kann nix, so wie du! Also klick mal schnell auf Beenden man.");
		lblAnsage.setBounds(54, 53, 676, 38);
		
		Font timesNewRoman = new Font("Times New Roman", Font.PLAIN + Font.ITALIC, 20);
		lblAnsage.setFont(timesNewRoman);
		
		contentPane.add(lblAnsage);
		
		JButton btnEnde = new JButton("Beenden");
		btnEnde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnEnde.setBounds(281, 146, 200, 28);
		btnEnde.setFont(timesNewRoman);
		contentPane.add(btnEnde);
		
	}

}
