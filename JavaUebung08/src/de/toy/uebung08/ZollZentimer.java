package de.toy.uebung08;

import java.awt.EventQueue;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.UIManager;

import java.text.DecimalFormat;

public class ZollZentimer extends JFrame {

	private JPanel contentPane;
	private JTextField tfZoll;
	private JLabel lblZentimeter;
	private JLabel lblErgebnisZentimeter;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ZollZentimer frame = new ZollZentimer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ZollZentimer() {
		setTitle("Umrechnung von Zoll in Zentimeter");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 447, 199);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblZoll = new JLabel("Zoll:");
		lblZoll.setBounds(10, 20, 50, 25);
		contentPane.add(lblZoll);
		
		JLabel lblZentimeter = new JLabel("Zentimeter:");
		lblZentimeter.setBounds(203, 20, 80, 25);
		contentPane.add(lblZentimeter);

		tfZoll = new JTextField();
		tfZoll.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					umrechnen();
				}
			}
		});
		tfZoll.setBounds(10, 45, 96, 25);
		contentPane.add(tfZoll);
		tfZoll.setColumns(10);
		
		JButton btnUmrechnen = new JButton("Umrechnen");
		btnUmrechnen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				umrechnen(); // Methode f�r Umrechnung durch Mausklick ausl�sen
			}
		});
		btnUmrechnen.setBounds(10, 99, 100, 25);
		contentPane.add(btnUmrechnen);
		
		JButton btnEnde = new JButton("Ende");
		btnEnde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnEnde.setBounds(199, 99, 100, 25);
		contentPane.add(btnEnde);
		
		lblErgebnisZentimeter = new JLabel("");
		lblErgebnisZentimeter.setBounds(203, 45, 96, 21);
		contentPane.add(lblErgebnisZentimeter);
	}

	public void umrechnen() {
		DecimalFormat f = new DecimalFormat("#0.00");
		var zoll = Double.parseDouble(tfZoll.getText());
		var Zentimeter = zoll * 2.54;
		lblErgebnisZentimeter.setText(f.format(Zentimeter) + " cm");
		tfZoll.requestFocus(); // h�lt den Fokus auf das Eingabefeld
		tfZoll.selectAll(); // markiert die Eingabe autom. f�r neue Eingaben ohne drauf zu klicken
	}
}
