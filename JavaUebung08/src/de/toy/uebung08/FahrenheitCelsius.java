package de.toy.uebung08;

import java.awt.BorderLayout;	// wird nicht mehr verwendet, da das Layout der contantPane-Komponente auf absolutLayout gesetzt wurde.
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.text.DecimalFormat;

public class FahrenheitCelsius extends JFrame {

	private JPanel contentPane;
	private JTextField tfFahrenheit;
	private JTextField tfCelsius;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FahrenheitCelsius frame = new FahrenheitCelsius();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FahrenheitCelsius() {
		setTitle("Umrechnung Fahrenheit in Celsius");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFahrenheit = new JLabel("Grad Fahrenheit");
		lblFahrenheit.setBounds(10, 10, 121, 14);
		contentPane.add(lblFahrenheit);
		
		JLabel lblCelsius = new JLabel("Grad Celsius");
		lblCelsius.setBounds(10, 80, 121, 14);
		contentPane.add(lblCelsius);
		
		tfFahrenheit = new JTextField();
		tfFahrenheit.addKeyListener(new KeyAdapter() {	// Listener, der auf Tastenbet�tigungen reagiert
			@Override	//w�rde auch ohne @Override funktionieren.
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					umrechnen();	// unten eigene Methode f�r die Umrechnung definiert
				}
			}
		});
		tfFahrenheit.setBounds(10, 34, 120, 30);
		contentPane.add(tfFahrenheit);
		tfFahrenheit.setColumns(10);
		
		tfCelsius = new JTextField();
		tfCelsius.setEditable(false);
		tfCelsius.setBounds(10, 104, 120, 30);
		contentPane.add(tfCelsius);
		tfCelsius.setColumns(10);
		
		JButton btnUmrechnen = new JButton("Umrechnen");
		btnUmrechnen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				umrechnen();	// unten eigene Methode f�r die Umrechnung definiert
			}	
		});
		btnUmrechnen.setBounds(205, 34, 100, 30);
		contentPane.add(btnUmrechnen);
		
		JButton btnEnde = new JButton("Schlie\u00DFen");
		btnEnde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnEnde.setBounds(205, 103, 100, 30);
		contentPane.add(btnEnde);
	}
	
	/** 
	 * Methode zur Umrechnung in Celsius
	 */
	
	public void umrechnen() {				
		var fahrenheit = Double.parseDouble(tfFahrenheit.getText());
		DecimalFormat f = new DecimalFormat("#0.00");
		var celsius = (fahrenheit - 32) * 5/9;
		tfCelsius.setText(f.format(celsius));
		//System.out.println(celsius); 	// zum testen
		tfFahrenheit.requestFocus();	// setzt und h�lt den Fokus auf das Eingabefeld f�r Fahrenheit
		tfFahrenheit.selectAll();		// markiert automatisch die Eingabe im Textfeld Fahrenheit
	}
}
