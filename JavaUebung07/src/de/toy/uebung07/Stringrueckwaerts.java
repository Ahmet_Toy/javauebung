package de.toy.uebung07;

import javax.swing.JOptionPane;

public class Stringrueckwaerts {

	public static void main(String[] args) {

		var eingabe = JOptionPane.showInputDialog("Geben Sie einen Text ein: ");
		System.out.println("Der eingegebene Text lautet: " + eingabe);

		for (int i = eingabe.length() - 1; i >= 0; i--) { // L�nge minus 1, da der Index von charAt bei 0 beginnt.
			System.out.print(eingabe.charAt(i)); // length-Methode liefert jedoch genau die L�nge des Wertes
		}
		int stringLaenge = eingabe.length();
		System.out.println("\nDie L�nge des Textes betr�gt " + stringLaenge + " Zeichen.");

		System.out.println("Buchstabe an der letzten Stelle im String: " + eingabe.charAt(stringLaenge - 1));

//------------------------------------------------------------------------------------------------------

		char letter = eingabe.charAt(0);
		String stringLetter = String.valueOf(letter); //Umwandlung von char zu String mit String valueOf()

		System.out.println("Umwandlung und Ausgabe des Wertes vom Typ char in einen String: " + stringLetter);
	}
}
