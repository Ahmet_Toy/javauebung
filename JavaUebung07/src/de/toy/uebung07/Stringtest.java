package de.toy.uebung07;

public class Stringtest {

	public static void main(String[] args) {
		var a = "Das ist";
		var b = "eine Zeichenkette";
		
		a = a.concat(b);			// diese Konkatenations-Methode verkettet zwei Strings miteinander und weist sie einer neuen or bestehenden Variable zu.
									// Konkatenation = Verkettung von Zeichenketten // Alternative ist der +-Operator 
		System.out.println(a);

	}

}
