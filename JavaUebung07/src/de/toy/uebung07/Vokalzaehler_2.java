package de.toy.uebung07;

import javax.swing.JOptionPane;

public class Vokalzaehler_2 {

	public static void main(String[] args) {
		int aZehler = 0, eZehler = 0, iZehler = 0, oZehler = 0, uZehler = 0;

		var eingabe = JOptionPane.showInputDialog("Geben Sie einen Text ein: ");
		var eingabeKlein = eingabe.toLowerCase();

		for (var i = 0; i < eingabe.length(); i++) {
			switch (eingabeKlein.charAt(i)) {
			case 'a':	// w�rde man �ber die Variable eingabe iterieren, w�rden gro�geschriebe Vokale nicht mitgez�hlt werden.
				aZehler++;
				break;
			case 'e':
				eZehler++;
				break;
			case 'i':
				iZehler++;
				break;
			case 'o':
				oZehler++;
				break;
			case 'u':
				uZehler++;
				break;
			}
		}
		System.out.println("Die Eingabe >> " + eingabe + " << enth�lt:");
		System.out.println(aZehler + " mal a");
		System.out.println(eZehler + " mal e");
		System.out.println(iZehler + " mal i");
		System.out.println(oZehler + " mal o");
		System.out.println(uZehler + " mal u");
	}
}