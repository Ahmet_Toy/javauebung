package de.toy.uebung07;

import javax.swing.JOptionPane;

public class String_und_Wrapper_Klassen {

	public static void main(String[] args) {

		System.out.println(Integer.valueOf("3F", 16)); // 3F zur Basis 16 umwandeln in eine Dezimalzahl

		System.out.println(Integer.valueOf("11101101", 2)); // 11101101 zur Basis 2 umwandeln in eine Dezimalzahl

		Double d = new Double("2.55");

		System.out.println(d);

		var eingabe = JOptionPane.showInputDialog("Bitte geben Sie eine Zahl ein:");
		System.out.println("String " + eingabe);

		System.out.println("Umwandlung von Stirng in Zahlenwert: " + Integer.parseInt(eingabe));

		var s = Integer.valueOf(eingabe);
		System.out.println("�ber Wrapper-Methode:" + s);

		int bin = 18;
		String str = Integer.toBinaryString(bin);
		System.out.println("Binary String: " + str);

		int octal = 58;
		System.out.println("Octal String: " + Integer.toOctalString(octal));

		int hex = 256;
		System.out.println("Hex String: " + Integer.toHexString(hex));

		int a = 5;
		int b = 2;

		int c = a * b;
		System.out.println("a * b = " + c);

		System.out.println("Obergrenze von Intger: " + Integer.MAX_VALUE);
		System.out.println("Untergrenze von Integer: " + Integer.MIN_VALUE);

		eingabe = JOptionPane.showInputDialog("Geben Sie eine Kommazahl ein: ");
		double d2 = Double.parseDouble(eingabe);
		Double d3 = new Double(0.0);
		System.out.println(d2 + "/" + d3 + "= " + d2 / d3);

		double d4 = 2.5;
		System.out.println(-d4 + "/" + 0 + "= " + -d4 / 0);
		System.out.println(0.0 + "/" + 0.0 + "= " + 0.0 / 0.0);
	}

}