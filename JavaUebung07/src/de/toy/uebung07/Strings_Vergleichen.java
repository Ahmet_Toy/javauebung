package de.toy.uebung07;

public class Strings_Vergleichen {

	public static void main(String[] args) {

		String frucht_a = new String("Apfel"); // Strings lassen sich auch �ber Konstruktor-Aufrufe instanziieren

		String frucht_b = "Birne"; // Oder direkt mit der Wert-Zuweisung instanziieren
		
		if (frucht_a == "Apfel") {
			System.out.println("Das ist ein Apfel\n");
		} else {
			System.out.println("Das ist eine Birne\n");	// Der Compiler wird "Birne" ausgeben !!!
		}
		
		String frucht_o = new String("Orange");
		String frucht_oo = new String("Orange");
		
		if (frucht_o == frucht_oo) {
			System.out.println("2 gleiche Orangen\n");
		} else {
			System.out.println("Orangen sind unterschiedlich\n");	// Der Compiler erkennt nicht, dass es sich um den selben String handelt
																	//, wenn die Strings �ber einen Konstruktor erzeugt wurden
		}
		
		String frucht_m = "Mango";
		String frucht_mm = "Mango";
		
		if (frucht_m == frucht_mm) {
			System.out.println("2 gleiche Mangos");	// Der Compiler erkennt jetzt, dass es sich um die selben Strings handelt, wenn die Strings
													// ohne Konstruktor erzeugt werden.
		} else {
			System.out.println("Mangos sind unterschiedlich");
		}
	}
}
