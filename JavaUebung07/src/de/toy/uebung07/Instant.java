package de.toy.uebung07;
import java.time.temporal.ChronoUnit;
//import java.time.Instant -- kann hier nicht als import deklariert werden, da diese Klasse auch Instant hei�t
import java.time.Duration;
import javax.swing.JOptionPane;

public class Instant {
	public static void main(String[] args) {
		
		long zeit = System.currentTimeMillis();					//Methode liefert zur�ck, wieviel Ms. seit EPOCH (=Nullpunkt)
		System.out.println("Zeit seit Epoch in ms: " + zeit);	//vergangen sind, welche als 01.01.1970, 00:00 Uhr definiert ist.
		
		Duration inDays = Duration.of(zeit,ChronoUnit.DAYS);
		System.out.println("Zeit seit Epoch in Tagen: " + inDays.toDays());
		//----------------------------------------------------------------------------------------------------------
		System.out.println("Epoch: " + java.time.Instant.EPOCH);	//zus�tzlich java.time. angeben, da Import gleich hei�t wie diese Klasse (Compiler hat sonst Namenskonflikt)
		
		java.time.Instant zeitpunkt = java.time.Instant.now();
		System.out.println("Aktuelle Systemzeit: " + zeitpunkt);
		java.time.Instant zeitpunktNano = java.time.Instant.parse("2021-11-23T15:29:20.721099800Z");
		System.out.println("Systemzeit mit Nanosekunden: " + zeitpunktNano);
		//----------------------------------------------------------------------------------------------------------
		java.time.Instant eingabeStart = java.time.Instant.now();
		JOptionPane.showInputDialog("Geben Sie Ihren Wohnort an: ");
		java.time.Instant eingabeEnde = java.time.Instant.now();
		Duration dauerEingabe = Duration.between(eingabeStart,eingabeEnde);
		
		System.out.println("Eingabe-Dauer nach ISO-8601: " + dauerEingabe);
		
		System.out.println("Eingabe-Dauer in Sekunden: " + dauerEingabe.getSeconds());
		System.out.println("Eingabe-Dauer in Minuten: " + dauerEingabe.toMinutes());
		System.out.println("Eingabe-Dauer in Millisek.: " + dauerEingabe.toMillis());
		
		//----------------------------------------------------------------------------------------------------------
		Duration dauer = Duration.of(10, ChronoUnit.SECONDS);
		Duration verlaengert = dauer.plus(20, ChronoUnit.MINUTES);
		
		System.out.println("Dauer: " + dauer);
		System.out.println("Verl�ngerte Dauer : " + verlaengert);
		System.out.println("Verl�ngerte Dauer in Sekunden: " + verlaengert.getSeconds());
		
		
	}

}
