package de.toy.uebung11;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.JButton;


public class Verschluesselung extends JFrame {

	private String dateiPfad = "." + File.separator + "verschluesselt.txt";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Verschluesselung frame = new Verschluesselung();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Verschluesselung() {
		setForeground(Color.GRAY);
		setTitle("Verschl\u00FCsselung");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 601, 385);
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		Font arial = new Font("Arial", Font.ITALIC + Font.PLAIN, 11);
		
		JLabel lblEingabe = new JLabel("Text-Eingabe");
		lblEingabe.setBounds(22, 10, 96, 18);
		lblEingabe.setFont(arial);
		contentPane.add(lblEingabe);
		
		JTextPane tpEingabe = new JTextPane();
		tpEingabe.setBounds(10, 27, 567, 231);
		tpEingabe.setFont(arial);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 27, 557, 231);
		contentPane.add(scrollPane);
		scrollPane.setViewportView(tpEingabe);
		
		JButton btnSpeichern = new JButton("Speichern");
		btnSpeichern.addActionListener(e -> {
				try (var out = new encryptedWriter(new FileWriter(dateiPfad))){
					out.write(tpEingabe.getText());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Fehler beim Speichern");
				}
	
		});
		btnSpeichern.setBounds(22, 268, 111, 21);
		btnSpeichern.setFont(arial);
		contentPane.add(btnSpeichern);
		
		JButton btnOpenEncrypted = new JButton("verschl\u00FCsselt \u00F6ffnen");
		btnOpenEncrypted.addActionListener(e ->{
			try (BufferedReader in = new BufferedReader(new FileReader(dateiPfad))){
				int c;
				var inhalt = new StringBuffer();
				while((c = in.read()) > 0) {	// Solange c ist gleich Anzahl der gelesenen Zeichen (in.read()) gr��er 0 ist, dann:
					inhalt.append((char) c);
				}
				tpEingabe.setText(inhalt.toString());
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Fehler beim Lesen der Datei!");
			}
		});
		
		btnOpenEncrypted.setBounds(271, 268, 221, 21);
		btnOpenEncrypted.setFont(arial);
		contentPane.add(btnOpenEncrypted);
		
		JButton btnOpenNormal = new JButton("unverschl\u00FCsselt \u00F6ffnen");
		btnOpenNormal.addActionListener(e ->{
			try(BufferedReader in = new BufferedReader(new encryptedReader(new FileReader(dateiPfad)))){
				int c;
				var inhalt = new StringBuffer();
				while((c = in.read()) > 0) {	// Solange c ist gleich Anzahl der gelesenen Zeichen (in.read()) gr��er 0 ist, dann:
					inhalt.append((char) c);
				}
				tpEingabe.setText(inhalt.toString());
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Fehler beim Lesen der Datei!");
			}
		});
		btnOpenNormal.setBounds(271, 305, 221, 21);
		btnOpenNormal.setFont(arial);
		contentPane.add(btnOpenNormal);
		
		JButton btnClose = new JButton("Schlie�en");
		btnClose.addActionListener(e ->{
			try {
				System.exit(0);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Fehler beim Schlie�en!");
			}
		});
		btnClose.setBounds(22, 305, 111, 21);
		btnClose.setFont(arial);
		contentPane.add(btnClose);
		
	}
}
