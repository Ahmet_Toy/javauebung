package de.toy.uebung11;

import java.awt.EventQueue;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JFileChooser;


public class Verzeichnisinhalt extends JFrame {
	
	private JPanel contentPane;
    private JTextField tfVerzeichnis;
    private File verzeichnis;
    private DefaultListModel<String> verzeichnisseModel, dateienModel;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    Verzeichnisinhalt frame = new Verzeichnisinhalt();
		    frame.setVisible(true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create the frame.
     */
    public Verzeichnisinhalt() {
    
    	setTitle("Verzeichnisinhalt");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 308);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnInhaltAnzeigen = new JButton("Inhalt anzeigen");
		btnInhaltAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verzeichnisseModel.clear();
				dateienModel.clear();
				if (tfVerzeichnis.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null,
							"Verzeichnisname fehlt!");
				} else {
					verzeichnis = new File(tfVerzeichnis.getText());
					if (verzeichnis.exists() && verzeichnis.isDirectory()) {
						String[] inhalt = verzeichnis.list();
						for (var eintrag : inhalt) {
							var f = new File(verzeichnis, eintrag);
							if (f.isDirectory()) {
								verzeichnisseModel.addElement(eintrag);
							} else {
								dateienModel.addElement(eintrag);
							}
						}
					} else {
						JOptionPane.showMessageDialog(null,
								"Verzeichnis existiert nicht!");
					}
				}
			}
		});
		
		JButton btnEnde = new JButton("Ende");
		btnEnde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnEnde.setBounds(335, 240, 89, 23);
		contentPane.add(btnEnde);
		
		JButton btnAuswaehlen = new JButton("Ausw�hlen...");
		btnAuswaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser auswahlfenster = new JFileChooser();	// JFileChooser ist eine Klasse, die ein Dialog-Fenster zur Auswahl von Dateien �ffnet.
				auswahlfenster.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int status = auswahlfenster.showOpenDialog(null);	// Methode, die das Dialog-Fenster anzeigt.
				if(status == JFileChooser.APPROVE_OPTION) {		// wenn das Auswahlfenster mit "�ffnen" oder "Speichern" beendet wurde, dann..
					File dateiAuswahl = auswahlfenster.getSelectedFile(); // erzeuge eine File und weise ihr die ausgew�hlte Datei zu 
					tfVerzeichnis.setText(dateiAuswahl.getPath()); // Setze in tfVerzeichnis die Pfadangabe der ausgew�hlten Datei ein
				}
			}
		});
		btnAuswaehlen.setBounds(315,56,109,23);
		contentPane.add(btnAuswaehlen);
		
		JLabel lblVerzeichnis = new JLabel("Verzeichnis");
		lblVerzeichnis.setBounds(10, 11, 161, 14);
		contentPane.add(lblVerzeichnis);
		
		tfVerzeichnis = new JTextField();
		tfVerzeichnis.setBounds(10, 25, 414, 20);
		contentPane.add(tfVerzeichnis);
		tfVerzeichnis.setColumns(10);
		btnInhaltAnzeigen.setBounds(10, 56, 161, 23);
		contentPane.add(btnInhaltAnzeigen);
		
		JLabel lblVerzeichnisse = new JLabel("Verzeichnisse");
		lblVerzeichnisse.setBounds(10, 90, 161, 14);
		contentPane.add(lblVerzeichnisse);
		
		JLabel lblDateien = new JLabel("Dateien");
		lblDateien.setBounds(229, 90, 133, 14);
		contentPane.add(lblDateien);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 105, 193, 125);
		contentPane.add(scrollPane);
		
		JList<String> listVerzeichnisse = new JList<String>();
		scrollPane.setViewportView(listVerzeichnisse);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(229, 105, 195, 125);
		contentPane.add(scrollPane_1);
		
		JList<String> listDateien = new JList<String>();
		scrollPane_1.setViewportView(listDateien);
		
		verzeichnisseModel = new DefaultListModel<>();
		listVerzeichnisse.setModel(verzeichnisseModel);
		dateienModel = new DefaultListModel<>();
		listDateien.setModel(dateienModel);
	}
}









